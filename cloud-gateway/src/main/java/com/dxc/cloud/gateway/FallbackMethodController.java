package com.dxc.cloud.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
//
//import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@RestController
public class FallbackMethodController {
	
//	@HystrixCommand(fallbackMethod = "userServiceFallbackMethod", commandProperties = {
//			   @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "4000")
//			})
	
	@GetMapping("/userServiceFallBack")
	public String userServiceFallBackMethod() {
		return "User Service is taking longer than expected." + " Please try again later.";
	}
	
	@GetMapping("/deptServiceFallBack")
	public String deptServiceFallBackMethod() {
		return "Department Service is taking longer than expected." + " Please try again later.";
	}
}
