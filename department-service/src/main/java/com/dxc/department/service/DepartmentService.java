package com.dxc.department.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.department.controller.DepartmentController;
import com.dxc.department.entity.Department;
import com.dxc.department.repository.DepartmentRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DepartmentService {	
	@Autowired
	private DepartmentRepository departmentRepo;

	public Department saveDepartment(Department department) {
		log.info("Inside saveDepartment of DepartmentService");
		return departmentRepo.save(department);
	}

	public Department findDepartmentByID(Long departmentId) {
		log.info("Inside findDepartmentByID of DepartmentService");
		return departmentRepo.findByDepartmentId(departmentId);
	}
}
